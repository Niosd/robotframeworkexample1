*** Settings ***
Documentation   This is just a simple example file
...
...             This was we can add more then one line of text
Default Tags    StayHome        StaySafe    #if no tag is given this will be added

***Variables***
${MYVAR}        John Doe

***Settings***
Force Tags      HomeWork        #this will be added no matter what
Suite Setup     log     The suit have started
Suite Teardown  log     The suit is finished
Test Setup      log     Lets start a test
Test Teardown   log     Huh...finished

***Test Cases***
# This is a comment
First test
    [Tags]  Seminar
    log     this is and example
    log     Some random message     warn
    comment     this string
    [Setup]     log     First test is warming up

Second test
    [Tags]  VerVal    Second tag is this
    log     this is also an example 
# This is also a comment

Variable test example
    log     ${MYVAR}
    log     My name is ${MYVAR}

***Variables***
${FirstName}    James
${LastName}     Bond
${test}     2

***Test Cases***

My name is
    log     ${LastName}
    log     ${FirstName} ${LastName}

    ${FULLNAME}=   Catenate     ${FirstName}   ${LastName}    
    log     I said ${FULLNAME}
    log     ${test}

Last test    
    log     this is also an example 
    ${test}=        Evaluate    9
    Set Global Variable   ${test}
    log     ${test}