*** Settings ***
Documentation   This is just a simple example file
...
...             This was we can add more then one line of text
Default Tags    StayHome        StaySafe    #if no tag is given this will be added
Force Tags      HomeWork        #this will be added no matter what

***Test Cases***
# This is a comment
First test
    [Tags]  Seminar
    log     this is and example
    log     Some random message     warn
    comment     this string

Second test
    [Tags]  VerVal    Second tag is this
    log     this is also an example 
# This is also a comment

Last test    
    log     this is also an example 