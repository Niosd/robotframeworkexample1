*** Settings ***
Documentation       A resource file with reusable keywords and variables.
Library             SeleniumLibrary

*** Variables ***
${USERNAME}  user1
${PASSWORD}  pass1
${DELAY}     0.5 seconds


*** Keywords ***
Open Browser To URL
    Open Browser    http://thedemosite.co.uk/index.php      browser=chrome
    Maximize Browser Window
    Set Selenium Speed      ${DELAY}

Go to login page
    Click Element   //a[contains(text(),'4. Login')]
    Title Should Be     Login example page to test the PHP MySQL online system

Input username
    [Arguments]     ${user}
    Input Text      //input[@name='username']   ${user}

Input pass
    [Arguments]     ${pass}
    Input Password  //input[@name='password']   ${pass}

Input credentials
    [Arguments]     ${user}     ${pass}
    Input username  ${user}
    Input pass      ${pass}

Sign Up Default User
    Sign Up User  ${USERNAME}  ${PASSWORD}

Sign Up User
    [Arguments]     ${name}     ${pass}
    Click Element   //a[contains(text(),'3. Add a User')]
    Input credentials  ${name}  ${pass}
    Click Element   //input[@value='save']

Login user
    [Arguments]     ${name}     ${pass}
    Go to login page
    Input credentials  ${name}  ${pass}    
    Click Element   //input[@value='Test Login']

Login successful
    Element Should Contain  //big//blockquote//blockquote   **Successful Login**

Login failed
    Element Should Contain  //big//blockquote//blockquote   **Failed Login**
