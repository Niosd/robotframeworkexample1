*** Settings ***
Documentation     A test suite with a single Gherkin style test.
Resource          resource.robot
Test Teardown     Close Browser
Test Setup        Open Browser To URL
 
*** Test Cases ***
Valid Login
    Given the user is registered
    When user "demo" logs in with password "mode"
    Then login should be sucessfull
 
*** Keywords ***
The user is registered
    Sign Up User  demo  mode
 
Login should be sucessfull
    Login successful

User "${username}" logs in with password "${password}"
    Login user  ${USERNAME}  ${password}